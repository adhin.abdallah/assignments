package assignments.assignment1;

import java.util.HashMap;
import java.util.Scanner;

public class ExtractNPM {

    //method untuk mengubah jumlah yang tadinya 2 digit menjadi 1 digit
    public static int sumValidator(int sum) {
        if(sum >= 10){
            String sumString = Integer.toString(sum);
            int newSum = Character.getNumericValue(sumString.charAt(0))+Character.getNumericValue(sumString.charAt(1));
            return sumValidator(newSum);
        }else{
            return sum;
        }
    }

    //method untuk menghitung kode npm
    public static boolean npmCodeCheck(String npm) {
        int sum1 = Character.getNumericValue(npm.charAt(0))*Character.getNumericValue(npm.charAt(12));
        int sum2 = Character.getNumericValue(npm.charAt(1))*Character.getNumericValue(npm.charAt(11));
        int sum3 = Character.getNumericValue(npm.charAt(2))*Character.getNumericValue(npm.charAt(10));
        int sum4 = Character.getNumericValue(npm.charAt(3))*Character.getNumericValue(npm.charAt(9));
        int sum5 = Character.getNumericValue(npm.charAt(4))*Character.getNumericValue(npm.charAt(8));
        int sum6 = Character.getNumericValue(npm.charAt(5))*Character.getNumericValue(npm.charAt(7));
        int sumValidator = sum1 + sum2 +sum3 + sum4 + sum5+ sum6 + Character.getNumericValue(npm.charAt(6));
        int validatingSum = sumValidator(sumValidator);
        if(validatingSum == Character.getNumericValue(npm.charAt(13))){
            return true;
        }
        return false;
    }

    //method untuk mengecek apakah umur dari mahasiswa/i >= 15 
    public static boolean ageChecker(String npm) {
         int birthYear = Integer.parseInt(npm.substring(8, 12));
         int currentYear = 2000 + Integer.parseInt(npm.substring(0, 2));
         if((currentYear - birthYear) >= 15) {
             return true;
         }else{
             return false;
         }
        }

    //method untuk mengecek apakah kode fakultas pada npm valid
    public static boolean facultyCodeChecker(String npm) {
        HashMap<String, String> mapOfFaculty = new HashMap<>();
        mapOfFaculty.put("01","Ilmu Komputer");
        mapOfFaculty.put("02","Sistem Informasi");
        mapOfFaculty.put("03","Teknologi Informasi");
        mapOfFaculty.put("11","Teknik Telekomunikasi");
        mapOfFaculty.put("12","Teknik Elektro");
        if(mapOfFaculty.containsKey(npm.substring(2, 4))){
            return true;
        }else{
            return false;
        }
    }

    //method untuk mengecek apakah npm valid atau tidak dengan semua syarat-syarat yang diberikan
    public static boolean validate(long npm) {
        String npmString = Long.toString(npm);
        if(npmString.length()==14 && facultyCodeChecker(npmString) && ageChecker(npmString) && npmCodeCheck(npmString)) {
            return true;
        }else {
            return false;
        }
    }

    //method untuk meng-extract informasi dari npm
    public static String extract(long npm) {
        String npmString = Long.toString(npm);
        HashMap<String, String> mapOfFaculty = new HashMap<>();
        mapOfFaculty.put("01","Ilmu Komputer");
        mapOfFaculty.put("02","Sistem Informasi");
        mapOfFaculty.put("03","Teknologi Informasi");
        mapOfFaculty.put("11","Teknik Telekomunikasi");
        mapOfFaculty.put("12","Teknik Elektro");
        StringBuffer entryYear = new StringBuffer(npmString.substring(0, 2));
        entryYear.insert(0,"20");
        String faculty = mapOfFaculty.get(npmString.substring(2, 4));
        StringBuffer dateOfBirth = new StringBuffer(npmString.substring(4, 12));
        dateOfBirth.insert(2, "-");
        dateOfBirth.insert(5, "-");
        return ("Tahun masuk: "+entryYear+"\nJurusan: "+faculty+"\nTanggal Lahir: "+dateOfBirth);
    }

    //main loop utama untuk meminta input dan menjalankan method-method untuk mengecek npm
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }else if(validate(npm)){
                System.out.println(extract(npm));
            }else{
                System.out.println("NPM tidak valid!");
            }         
        }
        input.close();
    }
}