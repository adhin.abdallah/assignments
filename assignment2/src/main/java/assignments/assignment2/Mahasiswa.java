package assignments.assignment2;

import java.util.Arrays;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private int jumlahMataKuliah;
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private String kode;
    private long npm;

    //constructor untuk class Mahasiswa
    public Mahasiswa(String nama, long npm) {
        this.nama = nama;
        this.npm = npm;
    }

    //method untuk mengambil total SKS
    public int getTotalSKS() {
        return this.totalSKS;
    }

    //method untuk mengambil NPM mahasiswa
    public long getNPM() {
        return this.npm;
    }

    //method untuk mengambil Array yang berisi mata kuliah yang diambil seorang mahasiswa
    public MataKuliah[] getMataKuliah() {
        return this.mataKuliah;
    }

    //method untuk menetapkan jurusan seorang mahasiswa atau mengambil jurusan mahasiswa tersebut
    public String getJurusan(long npm) {
        if(this.jurusan == null) {
            String facultyCodeString = Long.toString(npm).substring(2, 4);
            if(facultyCodeString.equals("01")) {
                this.jurusan = "Ilmu Komputer";
                this.kode = "IK";
            } else if(facultyCodeString.equals("02")){
                this.jurusan = "Sistem Informasi";
                this.kode = "SI";
            }
            return this.jurusan;
        } else {
            return this.jurusan;
        }
    }

    //method untuk mengambil jumlah matkul yang diambil seorang mahasiswa
    public int getJumlahMatkul() {
        return this.jumlahMataKuliah;
    }
    
    //method untuk add matkul
    public void addMatkul(MataKuliah mataKuliah) {
        if(!Arrays.asList(this.mataKuliah).contains(mataKuliah)) {
            if(mataKuliah.getJumlahMahasiswa()+1 <= mataKuliah.getKapasitas()) {
                if(this.jumlahMataKuliah+1 <= 10) {
                    for(int i = 0; i<10; i++) {
                        if(this.mataKuliah[i] == null) {
                            this.mataKuliah[i] = mataKuliah;
                            this.jumlahMataKuliah += 1;
                            this.totalSKS += mataKuliah.getSKS();
                            mataKuliah.addMahasiswa(this);
                            return;
                        }
                    }
                } else {
                    System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10.");
                }
            } else {
                System.out.println(String.format("[DITOLAK] %s telah penuh kapasitasnya.",mataKuliah.getNama()));
            }
        } else {
            System.out.println("[DITOLAK] " + mataKuliah.getNama()+" telah diambil sebelumnya.");
        }
    }

    //method untuk drop matkul
    public void dropMatkul(MataKuliah mataKuliah) {
        if(this.jumlahMataKuliah == 0) {
            System.out.println("[DITOLAK] Belum ada matkul yang diambil.");
        } else if(Arrays.asList(this.mataKuliah).contains(mataKuliah)) {
            for(int i = 0; i<10; i++) {
                if(this.mataKuliah[i] == mataKuliah) {
                    this.mataKuliah[i] = null;
                    this.jumlahMataKuliah -= 1;
                    this.totalSKS -= mataKuliah.getSKS();
                    mataKuliah.dropMahasiswa(this);
                        }
                    }
                } else {
                    System.out.println("[DITOLAK] " + mataKuliah.getNama()+" belum pernah diambil.");
            }
        }

    //method untuk mengecek IRS
    public void cekIRS() {
        int errorcounter = 0;
        masalahIRS = new String[this.jumlahMataKuliah+1];
        if(totalSKS>24) {
            this.masalahIRS[errorcounter] = String.format("SKS yang Anda ambil lebih dari 24");
            errorcounter += 1;
        }
        if(this.jumlahMataKuliah != 0) {
            for(MataKuliah matkul : this.mataKuliah) {
                if(matkul != null) {
                    if(matkul.getKode().equals("CS")) {
                        continue;
                    }else {
                    if(!matkul.getKode().equals(this.kode)) {
                        this.masalahIRS[errorcounter] = String.format("Mata Kuliah %s tidak dapat diambil jurusan %s", matkul.getNama(), this.kode);
                        errorcounter += 1;
                        }
                    }
                }
            }
        }
        if(errorcounter == 0) {
            System.out.println("IRS tidak bermasalah.");
        }
        for(int i = 0; i<this.jumlahMataKuliah+1; i++) {
            if(masalahIRS[i] != null) {
                System.out.println((i+1) +". " + masalahIRS[i]);
            }
        }
    }

    //representasi class mahasiswa dalam bentuk string
    public String toString() {
        return this.nama;
    }

}