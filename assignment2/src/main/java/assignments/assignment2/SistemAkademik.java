package assignments.assignment2;

import java.util.Scanner;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    
    private Scanner input = new Scanner(System.in);

    //method untuk mengambil mahasiswa dari sistem akademik
    private Mahasiswa getMahasiswa(long npm) {
        for(Mahasiswa mSiswa : SistemAkademik.daftarMahasiswa) {
            if(mSiswa.getNPM() == npm) {
                return mSiswa; 
            }
        }
        return null;
    }

    //method untuk mengambil matkul dari sistem akademik
    private MataKuliah getMataKuliah(String namaMataKuliah) {
        for(MataKuliah matkul : SistemAkademik.daftarMataKuliah) {
            if(matkul.getNama().equals(namaMataKuliah)){
                return matkul;
            }
        }
        return null;
    }

    //method untuk menambahkan suatu matkul yang diambil seorang mahasiswa
    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

        System.out.print("Banyaknya Matkul yang Ditambah: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang ditambah");
        for(int i=0; i<banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i+1 + " : ");
            String namaMataKuliah = input.nextLine();
            this.getMahasiswa(npm).addMatkul(this.getMataKuliah(namaMataKuliah));
        }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    //method untuk men-drop matkul dari matkul yang sudah diambil seorang mahasiswa
    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());
        if(this.getMahasiswa(npm).getJumlahMatkul() == 0) {
            System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");
            return;
        }

        System.out.print("Banyaknya Matkul yang Di-drop: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan nama matkul yang di-drop:");
        for(int i=0; i<banyakMatkul; i++){
            System.out.print("Nama matakuliah " + i+1 + " : ");
            String namaMataKuliah = input.nextLine();
                this.getMahasiswa(npm).dropMatkul(this.getMataKuliah(namaMataKuliah));
            }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
    }

    //method untuk mencetak ringkasan mahasiswa sesuai format
    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());
        Mahasiswa searchedMahasiswa = this.getMahasiswa(npm);

        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama: " + searchedMahasiswa);
        System.out.println("NPM: " + npm);
        System.out.println("Jurusan: " + searchedMahasiswa.getJurusan(npm));
        System.out.println("Daftar Mata Kuliah: ");

        if(searchedMahasiswa.getJumlahMatkul() == 0) {
            System.out.println("Belum ada mata kuliah yang diambil");
        } else {
            int counter = 0; 
            for(MataKuliah matkul : searchedMahasiswa.getMataKuliah()) {
                if(matkul != null){
                    System.out.println((counter+1) + ". " + matkul);
                    counter += 1;
                }
            }
        }
        System.out.println("Total SKS: " + searchedMahasiswa.getTotalSKS());
        
        System.out.println("Hasil Pengecekan IRS:");

        searchedMahasiswa.cekIRS();
    }

    //method untuk mencetak ringkasan mata kuliah sesuai format
    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();
        MataKuliah searchedMataKuliah = this.getMataKuliah(namaMataKuliah);

        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + searchedMataKuliah);
        System.out.println("Kode: " + searchedMataKuliah.getKode());
        System.out.println("SKS: " + searchedMataKuliah.getSKS());
        System.out.println("Jumlah mahasiswa: " + searchedMataKuliah.getJumlahMahasiswa());
        System.out.println("Kapasitas: " + searchedMataKuliah.getKapasitas());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");

        if(searchedMataKuliah.getJumlahMahasiswa() == 0) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");
        }else {
            int counter = 0;
            for(Mahasiswa mSiswa : searchedMataKuliah.getDaftarMahasiswa()) {
                if(mSiswa != null) {
                    System.out.println(counter+1 + ". " + mSiswa);
                    counter+=1;
                }
            }
        }
    }

    //method untuk mencetak menu utama
    //serta memilih perintah yang ingin dijalankan
    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }

    }

    //method untuk inisiasi awal sistem akademik
    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        //menambahkan matkul ke dalam sistem akademik
        for(int i=0; i<banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            SistemAkademik.daftarMataKuliah[i] = new MataKuliah(dataMatkul[0],dataMatkul[1], sks, kapasitas);
        }

        //menambahkan matkul ke dalam sistem akademik
        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i=0; i<banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            long npm = Long.parseLong(dataMahasiswa[1]);
            SistemAkademik.daftarMahasiswa[i] = new Mahasiswa(dataMahasiswa[0], npm);
        }
        daftarMenu();
        input.close();
    }

    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }
}