package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int jumlahMahasiswa;

    //constructor untuk class Matakuliah
    public MataKuliah(String kode, String nama, int sks, int kapasitas) {
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[this.kapasitas];
    }

    //method untuk mengambil kode matkul
    public String getKode() {
        return this.kode;
    }

    //method untuk mengambil nama matkul
    public String getNama() {
        return this.nama;
    }

    //method untuk mengambil SKS matkul
    public int getSKS() {
        return this.sks;
    }

    //method untuk mengambil kapasitas matkul
    public int getKapasitas() {
        return this.kapasitas;
    }

    //method untuk mengambil Array yang berisi mahasiswa yang telah mengambil suatu matkul
    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }

    //method untuk mengambil total SKS
    public int getJumlahMahasiswa() {
        return this.jumlahMahasiswa;
    }

    //method untuk menambahkan mahasiswa yang mengambil suatu matkul 
    public void addMahasiswa(Mahasiswa mahasiswa) {
        for(int i = 0; i<this.getKapasitas(); i++) {
            if(this.daftarMahasiswa[i] == null) {
                this.daftarMahasiswa[jumlahMahasiswa] = mahasiswa;
                this.jumlahMahasiswa += 1;
                return;
            }
        }
    }

    //method untuk menghilangkan mahasiswa dari Array jika mahasiswa tersebut drop matkul
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        for(int i = 0; i<this.kapasitas; i++) {
            if(this.daftarMahasiswa[i] == mahasiswa) {
                this.daftarMahasiswa[i] = null;
                this.jumlahMahasiswa -= 1;
            }
        }
    }

    //representasi string dari class MataKuliah
    public String toString() {
        return this.nama;
    }
}