package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI extends JPanel{

    protected static JComboBox matkulMenu;

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // menentukan Layout dari RingkasanMataKuliahGUI
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setAlignmentY(Component.CENTER_ALIGNMENT);

        // inisialisasi label titleLabel
        JLabel titleLabel = new JLabel("Ringkasan Mata Kuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // inisialisasi label pilihMatkul
        JLabel pilihMatkul = new JLabel();
        pilihMatkul.setText("Pilih Nama Matkul");
        pilihMatkul.setHorizontalAlignment(JLabel.CENTER);
        pilihMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        pilihMatkul.setAlignmentY(Component.CENTER_ALIGNMENT);
        pilihMatkul.setFont(SistemAkademikGUI.fontGeneral);

        // inisialisasi tombol showButton
        JButton showButton = new JButton();
        showButton.setFont(SistemAkademikGUI.fontGeneral);
        showButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        showButton.setAlignmentY(Component.CENTER_ALIGNMENT);
        showButton.setBackground(Color.decode("#9cc634"));
        showButton.setForeground(Color.WHITE);
        // inisialisasi lihatMatkul yang merupakan action yang akan ditambahkan ke tombolnya 
        // dan di-bind ke enter key
        AbstractAction lihatMatkul = new AbstractAction("Lihat"){
            public void actionPerformed(ActionEvent e) {

                String matkul = (String) matkulMenu.getSelectedItem();
                
                if(matkul == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field - RingkasanMatkul");
                    return;
                }
                MataKuliah m = this.getMataKuliah(matkul);
                DetailRingkasanMataKuliahGUI ringkasan = new DetailRingkasanMataKuliahGUI(frame, m, daftarMahasiswa, daftarMataKuliah);
                HomeGUI.masterPanel.add(ringkasan, "detail ringkasan matkul");
                CardLayout cl = (CardLayout) HomeGUI.masterPanel.getLayout();
                cl.show(HomeGUI.masterPanel, "detail ringkasan matkul");

            }
            private MataKuliah getMataKuliah(String nama) {
                for (MataKuliah mataKuliah : SistemAkademikGUI.daftarMataKuliah) {
                    if (mataKuliah.getNama().equals(nama)){
                        return mataKuliah;
                    }
                }
                return null;
            }
        };

        // manambahkan action lihatMatkul ke showbutton
        showButton.setAction(lihatMatkul);
        // mem-bind enter key dengan lihatMatkul
        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "lihat matkul");
        this.getActionMap().put("lihat matkul", lihatMatkul);

        // inisialisasi Combobox matkulMenu
        Object[] dftrMatkul = daftarMataKuliah.toArray();
        matkulMenu = new JComboBox<>(dftrMatkul);
        matkulMenu.setMaximumSize(new Dimension(showButton.getPreferredSize().width, pilihMatkul.getPreferredSize().height+6));

        // inisialisasi tombol backButton
        JButton backButton = new JButton("Kembali");
        backButton.setFont(SistemAkademikGUI.fontGeneral);
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setAlignmentY(Component.CENTER_ALIGNMENT);
        backButton.setBackground(Color.decode("#84aecc"));
        backButton.setForeground(Color.WHITE);
        // inisialisasi back yang merupakan action yang akan ditambahkan ke tombolnya 
        // dan di-bind ke escape key
        AbstractAction back = new AbstractAction("Kembali") {
            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) HomeGUI.masterPanel.getLayout();
                cl.show(HomeGUI.masterPanel, "home panel");
            }
        };
        backButton.setAction(back);
        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), "kembali");
        this.getActionMap().put("kembali", back);

        // menambahkan komponen-komponen ke RingkasanMataKuliahGUI
        this.add(titleLabel);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(pilihMatkul);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(matkulMenu);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(showButton);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(backButton);
    }

    // method untuk mengupdate value yang ditunjukkan matkulMenu
    public static void UpdateMatkulMenu() {
        ArrayList<MataKuliah> arr1 = SistemAkademikGUI.daftarMataKuliah;
        DefaultComboBoxModel model = (DefaultComboBoxModel) RingkasanMataKuliahGUI.matkulMenu.getModel();
        model.removeAllElements();

        for (MataKuliah e : arr1) {
            model.addElement(e.toString());
        }
        RingkasanMataKuliahGUI.matkulMenu.getModel();
    }
}