package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI extends JPanel{
    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // mengambil data-data yang diperlukan
        String nama = mahasiswa.getNama();
        String npm = mahasiswa.toString();
        String jurusan = mahasiswa.getJurusan();
        int sks = mahasiswa.getTotalSKS();
        MataKuliah[] matkul = mahasiswa.getMataKuliah();
        String[] masalahIRS = mahasiswa.getMasalahIRS();

        // menentukan Layout dari DetailRingkasanMahasiswaGUI
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setAlignmentY(Component.CENTER_ALIGNMENT);

        // inisialisasi label titleLabel
        JLabel titleLabel = new JLabel("Detail Ringkasan Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(titleLabel);

        // inisialisasi label name
        JLabel name = new JLabel("Nama: " + nama);
        name.setHorizontalAlignment(JLabel.CENTER);
        name.setAlignmentX(Component.CENTER_ALIGNMENT);
        name.setAlignmentY(Component.CENTER_ALIGNMENT);
        name.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(name);

        // inisialisasi label npmLabel
        JLabel npmLabel = new JLabel("NPM: " + npm);
        npmLabel.setHorizontalAlignment(JLabel.CENTER);
        npmLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        npmLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        npmLabel.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(npmLabel);

        // inisialisasi label jurusanLabel
        JLabel jurusanLabel = new JLabel("Jurusan: " + jurusan);
        jurusanLabel.setHorizontalAlignment(JLabel.CENTER);
        jurusanLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        jurusanLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        jurusanLabel.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(jurusanLabel);

        // inisialisasi label daftarMatkulLabel
        JLabel daftarMatkulLabel = new JLabel("Daftar Mata Kuliah:");
        daftarMatkulLabel.setHorizontalAlignment(JLabel.CENTER);
        daftarMatkulLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        daftarMatkulLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        daftarMatkulLabel.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(daftarMatkulLabel);

        // if else untuk menentukan label mana yang akan ditambahkan
        if(mahasiswa.getBanyakMatkul() == 0) {
            // inisialisasi label noMatkul
            JLabel noMatkul = new JLabel("Belum ada mata kuliah yang diambil.");
            noMatkul.setHorizontalAlignment(JLabel.CENTER);
            noMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
            noMatkul.setAlignmentY(Component.CENTER_ALIGNMENT);
            noMatkul.setFont(new Font("Century Gothic", Font.BOLD , 14));
            this.add(Box.createRigidArea(new Dimension(5,10)));
            this.add(noMatkul);
        }
        else {
            int counter = 0; 
            // for loop untuk inisiasi dan penambahan
            // label-label yang berisi nama matkul yang telah diambil
            for(MataKuliah e : matkul) {
                if(e != null){
                    String matkulString = (counter+1) + ". " + e;
                    counter += 1;

                    JLabel matkulList = new JLabel(matkulString);
                    matkulList.setAlignmentX(Component.CENTER_ALIGNMENT);
                    matkulList.setAlignmentY(Component.CENTER_ALIGNMENT);
                    matkulList.setFont(new Font("Century Gothic", Font.BOLD , 14));
                    this.add(Box.createRigidArea(new Dimension(5,10)));
                    this.add(matkulList);
                }
            }    
        }

        // inisialisasi label totalSKS
        JLabel totalSKS = new JLabel("Total SKS: " + sks);
        totalSKS.setAlignmentX(Component.CENTER_ALIGNMENT);
        totalSKS.setAlignmentY(Component.CENTER_ALIGNMENT);
        totalSKS.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(totalSKS);

        // inisialisasi label cekIRS
        JLabel cekIRS = new JLabel("Hasil Pengecekan IRS:");
        cekIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        cekIRS.setAlignmentY(Component.CENTER_ALIGNMENT);
        cekIRS.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(cekIRS);

        // if else untuk menentukan label mana yang akan ditambahkan
        if(mahasiswa.getBanyakMasalahIRS() == 0) {
            // inisiasi label noProblem
            JLabel noProblem = new JLabel("IRS tidak bermasalah.");
            noProblem.setHorizontalAlignment(JLabel.CENTER);
            noProblem.setAlignmentX(Component.CENTER_ALIGNMENT);
            noProblem.setAlignmentY(Component.CENTER_ALIGNMENT);
            noProblem.setFont(new Font("Century Gothic", Font.BOLD , 14));
            this.add(Box.createRigidArea(new Dimension(5,10)));
            this.add(noProblem);
        }
        else {
            // for loop untuk inisiasi dan penambahan
            // label-label yang berisi masalah IRS
            for(int i = 0; i<masalahIRS.length; i++) {
                if(masalahIRS[i] != null) {
                    String problemString = ((i+1) +". " + masalahIRS[i]);

                    JLabel matkulList = new JLabel(problemString);
                    matkulList.setAlignmentX(Component.CENTER_ALIGNMENT);
                    matkulList.setAlignmentY(Component.CENTER_ALIGNMENT);
                    matkulList.setFont(new Font("Century Gothic", Font.BOLD , 14));
                    this.add(Box.createRigidArea(new Dimension(5,10)));
                    this.add(matkulList);
                }
            }
        }

        // inisiasi tombol doneButton
        JButton doneButton = new JButton("Selesai");
        doneButton.setFont(SistemAkademikGUI.fontGeneral);
        doneButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        doneButton.setAlignmentY(Component.CENTER_ALIGNMENT);
        doneButton.setBackground(Color.decode("#9cc634"));
        doneButton.setForeground(Color.WHITE);
        doneButton.addActionListener( new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) HomeGUI.masterPanel.getLayout();
                cl.show(HomeGUI.masterPanel, "home panel");
            }
        });
        // membind enter key untuk meng-click tombol doneButton
        // tetapi gagal implementasinya karena walaupun sudah menggunkaan .grabFocus
        // fous tetap tidak berpindah ke doneButton
        doneButton.addKeyListener( new KeyListener() {
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                    doneButton.doClick();
                }
            }

            public void keyTyped(KeyEvent e) {
           
            }

            public void keyReleased(KeyEvent e) {
          
            }
        });
        doneButton.grabFocus();
        
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(doneButton);  
    }
}
