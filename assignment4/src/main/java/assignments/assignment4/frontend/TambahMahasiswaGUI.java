package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI extends JPanel{

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
       
        // menentukan layout dari TambahMahasiswaGUI
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setAlignmentY(Component.CENTER_ALIGNMENT);

        // inisialisasi label titleLabel
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // inisialisasi label namaLabel
        JLabel namaLabel = new JLabel();
        namaLabel.setText("Nama:");
        namaLabel.setHorizontalAlignment(JLabel.CENTER);
        namaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        namaLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        namaLabel.setFont(SistemAkademikGUI.fontGeneral);

        // inisialisasi textfield untuk memasukkan nama
        JTextField namaField = new JTextField();
        namaField.setHorizontalAlignment(JLabel.CENTER);
        namaField.setMaximumSize(new Dimension(titleLabel.getPreferredSize().width, namaField.getPreferredSize().height));

        // inisialisasi label npmLabel
        JLabel npmLabel = new JLabel();
        npmLabel.setText("NPM:");
        npmLabel.setHorizontalAlignment(JLabel.CENTER);
        npmLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        npmLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        npmLabel.setFont(SistemAkademikGUI.fontGeneral);

        // inisialisasi textfield untuk memasukkan npm
        JTextField npmField = new JTextField();
        npmField.setHorizontalAlignment(JLabel.CENTER);
        npmField.setMaximumSize(new Dimension(titleLabel.getPreferredSize().width, namaField.getPreferredSize().height));

        // inisialisasi tombol addButton
        JButton addButton = new JButton();
        addButton.setFont(SistemAkademikGUI.fontGeneral);
        addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addButton.setAlignmentY(Component.CENTER_ALIGNMENT);
        addButton.setBackground(Color.decode("#9cc634"));
        addButton.setForeground(Color.WHITE);
        // inisialisasi tambahMahasiswa yang merupakan action yang akan ditambahkan ke tombolnya 
        // dan di-bind ke enter key
        AbstractAction tambahMahasiswa = new AbstractAction("Tambahkan"){
            public void actionPerformed(ActionEvent e) {

                String nama = namaField.getText();
                String npm = npmField.getText();
                
                if(npm.equals("") || nama.equals("")) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field - TambahMahasiswa");
                    return;
                }
                Long npmLong = Long.parseLong(npm);
                Mahasiswa student = new Mahasiswa(nama, npmLong);
                if(daftarMahasiswa.contains(student)) {
                    JOptionPane.showMessageDialog(frame, String.format("NPM %d sudah pernah ditambahkan sebelumnya", npmLong));
                    namaField.setText("");
                    npmField.setText("");
                }
                else {
                    daftarMahasiswa.add(student);
                    SistemAkademikGUI.sortMahasiswa();
                    TambahIRSGUI.UpdateStudentMenu();
                    RingkasanMahasiswaGUI.UpdateStudentMenu();
                    HapusIRSGUI.UpdateStudentMenu();
                    JOptionPane.showMessageDialog(frame, String.format("Mahasiswa %d-%s berhasil ditambahkan", npmLong, nama));
                    namaField.setText("");
                    npmField.setText("");
                }
            }
        };
        // manambahkan action tambahMahasiswa ke addButton
        addButton.setAction(tambahMahasiswa);
        // mem-bind enter key dengan tambahMahasiswa
        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "tambah mahasiswa");
        this.getActionMap().put("tambah mahasiswa",tambahMahasiswa);

        // inisialisasi tombol backButton
        JButton backButton = new JButton("Kembali");
        backButton.setFont(SistemAkademikGUI.fontGeneral);
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setAlignmentY(Component.CENTER_ALIGNMENT);
        backButton.setBackground(Color.decode("#84aecc"));
        backButton.setForeground(Color.WHITE);
        // inisialisasi back yang merupakan action yang akan ditambahkan ke tombolnya 
        // dan di-bind ke escape key
        AbstractAction back = new AbstractAction("Kembali") {
            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) HomeGUI.masterPanel.getLayout();
                cl.show(HomeGUI.masterPanel, "home panel");
            }
        };
        backButton.setAction(back);
        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), "kembali");
        this.getActionMap().put("kembali", back);

        // menambahkan komponen-komponen ke TambahMahasiswaGUI
        this.add(Box.createRigidArea(new Dimension(5,50)));
        this.add(titleLabel);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(namaLabel);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(namaField);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(npmLabel);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(npmField);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(addButton);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(backButton);
    }
}