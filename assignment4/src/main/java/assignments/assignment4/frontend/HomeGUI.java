package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI {

    public static JPanel masterPanel;
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // inisialisasi masterPanel dengan CardLayout() 
        masterPanel = new JPanel();
        masterPanel.setLayout(new CardLayout());
        
        // inisialisasi label judul
        JLabel titleLabel = new JLabel("Selamat datang di Sistem Akademik");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setAlignmentY(Component.CENTER_ALIGNMENT);

        // inisialisasi panel home
        JPanel homePanel = new JPanel();
        homePanel.setAlignmentY(Component.CENTER_ALIGNMENT);
        homePanel.setLayout(new BoxLayout(homePanel, BoxLayout.Y_AXIS));

        // inisialisasi tombol tambah mahasiswa
        JButton tambahMahasiswa = new JButton("Tambah Mahasiswa");
        tambahMahasiswa.setFont(SistemAkademikGUI.fontGeneral);
        tambahMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahMahasiswa.setAlignmentY(Component.CENTER_ALIGNMENT);
        tambahMahasiswa.setBackground(Color.decode("#9cc634"));
        tambahMahasiswa.setForeground(Color.WHITE);
        //mengimplementasikan actionlistener agar tombol tambah mahasiswa bisa menjalankan fungsinya
        tambahMahasiswa.addActionListener( new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) masterPanel.getLayout();
                cl.show(masterPanel, "tambah mahasiswa");
            }
        });

        // inisialisasi tombol tambah mata kuliah
        JButton tambahMatKul = new JButton("Tambah Mata Kuliah");
        tambahMatKul.setFont(SistemAkademikGUI.fontGeneral);
        tambahMatKul.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahMatKul.setAlignmentY(Component.CENTER_ALIGNMENT);
        tambahMatKul.setBackground(Color.decode("#9cc634"));
        tambahMatKul.setForeground(Color.WHITE);
        tambahMatKul.addActionListener( new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) masterPanel.getLayout();
                cl.show(masterPanel, "tambah matkul");
            }
        });

        // inisialisasi tombol tambahIRS
        JButton tambahIRS = new JButton("Tambah IRS");
        tambahIRS.setFont(SistemAkademikGUI.fontGeneral);
        tambahIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahIRS.setAlignmentY(Component.CENTER_ALIGNMENT);
        tambahIRS.setBackground(Color.decode("#9cc634"));
        tambahIRS.setForeground(Color.WHITE);
        tambahIRS.addActionListener( new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) masterPanel.getLayout();
                cl.show(masterPanel, "tambah irs");
            }
        });

        // inisialisasi tombol HapusIRS
        JButton hapusIRS = new JButton("Hapus IRS");
        hapusIRS.setFont(SistemAkademikGUI.fontGeneral);
        hapusIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        hapusIRS.setAlignmentY(Component.CENTER_ALIGNMENT);
        hapusIRS.setBackground(Color.decode("#9cc634"));
        hapusIRS.setForeground(Color.WHITE);
        hapusIRS.addActionListener( new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) masterPanel.getLayout();
                cl.show(masterPanel, "hapus irs");
            }
        });

        // inisialisasi tombol lihatRingkasanMsiswa
        JButton lihatRingkasanMsiswa = new JButton("Lihat Ringkasan Mahasiswa");
        lihatRingkasanMsiswa.setFont(SistemAkademikGUI.fontGeneral);
        lihatRingkasanMsiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        lihatRingkasanMsiswa.setAlignmentY(Component.CENTER_ALIGNMENT);
        lihatRingkasanMsiswa.setBackground(Color.decode("#9cc634"));
        lihatRingkasanMsiswa.setForeground(Color.WHITE);
        lihatRingkasanMsiswa.addActionListener( new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) masterPanel.getLayout();
                cl.show(masterPanel, "ringkasan mahasiswa");
            }
        });

        // inisialisasi tombol lihatRingkasanMatkul
        JButton lihatRingkasanMatkul = new JButton("Lihat Ringkasan Mata Kuliah");
        lihatRingkasanMatkul.setFont(SistemAkademikGUI.fontGeneral);
        lihatRingkasanMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        lihatRingkasanMatkul.setAlignmentY(Component.CENTER_ALIGNMENT);
        lihatRingkasanMatkul.setBackground(Color.decode("#9cc634"));
        lihatRingkasanMatkul.setForeground(Color.WHITE);
        lihatRingkasanMatkul.addActionListener( new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) masterPanel.getLayout();
                cl.show(masterPanel, "ringkasan matkul");
            }
        });

        AbstractAction betterUX = new AbstractAction(){
            @Override
            public void actionPerformed(ActionEvent e) {
                if(tambahMahasiswa.isFocusOwner()) {
                    tambahMahasiswa.doClick();
                }
                else if(tambahMatKul.isFocusOwner()) {
                    tambahMatKul.doClick();
                }
                else if(tambahIRS.isFocusOwner()) {
                    tambahIRS.doClick();
                }
                else if(hapusIRS.isFocusOwner()) {
                    hapusIRS.doClick();
                }
                else if(lihatRingkasanMsiswa.isFocusOwner()) {
                    lihatRingkasanMsiswa.doClick();
                }
                else if(lihatRingkasanMatkul.isFocusOwner()) {
                    lihatRingkasanMatkul.doClick();
                }
            }
        };
        tambahMahasiswa.grabFocus();
        homePanel.getInputMap(2).put(KeyStroke.getKeyStroke("ENTER"), "click a button");
        homePanel.getActionMap().put("click a button", betterUX);

        //  menambahkan komponen-komponen homePanel
        homePanel.add(titleLabel);
        homePanel.add(Box.createRigidArea(new Dimension(5,10)));
        homePanel.add(tambahMahasiswa);
        homePanel.add(Box.createRigidArea(new Dimension(5,10)));
        homePanel.add(tambahMatKul);
        homePanel.add(Box.createRigidArea(new Dimension(5,10)));
        homePanel.add(tambahIRS);
        homePanel.add(Box.createRigidArea(new Dimension(5,10)));
        homePanel.add(hapusIRS);
        homePanel.add(Box.createRigidArea(new Dimension(5,10)));
        homePanel.add(lihatRingkasanMsiswa);
        homePanel.add(Box.createRigidArea(new Dimension(5,10)));
        homePanel.add(lihatRingkasanMatkul);
        homePanel.add(Box.createRigidArea(new Dimension(5,10)));

        // menambahkan semua panel pada masterPanel
        masterPanel.add(homePanel, "home panel");
        masterPanel.add(new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah), "tambah mahasiswa") ;
        masterPanel.add(new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah), "tambah matkul");
        masterPanel.add(new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah), "tambah irs");
        masterPanel.add(new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah), "hapus irs");
        masterPanel.add(new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah), "ringkasan mahasiswa");
        masterPanel.add(new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah), "ringkasan matkul");
        
        // menentukan layout dari frame utama
        frame.setLayout(new GridBagLayout());
        frame.add(masterPanel, new GridBagConstraints());
    }
}
