package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI extends JPanel{

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // menentukan Layout dari TambahMataKuliahGUI
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setAlignmentY(Component.CENTER_ALIGNMENT);

        // inisialisasi label titleLabel
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mata Kuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // inisialisasi label kodeMatkulLabel
        JLabel kodeMatkulLabel = new JLabel();
        kodeMatkulLabel.setText("Kode Mata Kuliah:");
        kodeMatkulLabel.setHorizontalAlignment(JLabel.CENTER);
        kodeMatkulLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        kodeMatkulLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        kodeMatkulLabel.setFont(SistemAkademikGUI.fontGeneral);

        // inisialisasi textfield untuk memasukkan kode matkul
        JTextField kodeMatkulField = new JTextField();
        kodeMatkulField.setHorizontalAlignment(JLabel.CENTER);
        kodeMatkulField.setMaximumSize(new Dimension(titleLabel.getPreferredSize().width, kodeMatkulField.getPreferredSize().height));

        // inisialisasi label namaMatkulLabel
        JLabel namaMatkulLabel = new JLabel();
        namaMatkulLabel.setText("Nama Mata Kuliah:");
        namaMatkulLabel.setHorizontalAlignment(JLabel.CENTER);
        namaMatkulLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        namaMatkulLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        namaMatkulLabel.setFont(SistemAkademikGUI.fontGeneral);

        // inisialisasi textfield untuk memasukkan nama matkul
        JTextField namaMatkulField = new JTextField();
        namaMatkulField.setHorizontalAlignment(JLabel.CENTER);
        namaMatkulField.setMaximumSize(new Dimension(titleLabel.getPreferredSize().width, kodeMatkulField.getPreferredSize().height));

        // inisialisasi label SKSLabel
        JLabel SKSLabel = new JLabel();
        SKSLabel.setText("SKS:");
        SKSLabel.setHorizontalAlignment(JLabel.CENTER);
        SKSLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        SKSLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        SKSLabel.setFont(SistemAkademikGUI.fontGeneral);

        // inisialisasi textfield untuk memasukkan sks
        JTextField SKSField = new JTextField();
        SKSField.setHorizontalAlignment(JLabel.CENTER);
        SKSField.setMaximumSize(new Dimension(titleLabel.getPreferredSize().width, kodeMatkulField.getPreferredSize().height));

        // inisialisasi label kapasitasLabel
        JLabel kapasitasLabel = new JLabel();
        kapasitasLabel.setText("Kapasitas:");
        kapasitasLabel.setHorizontalAlignment(JLabel.CENTER);
        kapasitasLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        kapasitasLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        kapasitasLabel.setFont(SistemAkademikGUI.fontGeneral);
        
        // inisialisasi text field untuk memasukkan kapasitas
        JTextField kapasitasField = new JTextField();
        kapasitasField.setHorizontalAlignment(JLabel.CENTER);
        kapasitasField.setMaximumSize(new Dimension(titleLabel.getPreferredSize().width, kodeMatkulField.getPreferredSize().height));

        // inisialisasi tombol addButton
        JButton addButton = new JButton("Tambahkan");
        addButton.setFont(SistemAkademikGUI.fontGeneral);
        addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addButton.setAlignmentY(Component.CENTER_ALIGNMENT);
        addButton.setBackground(Color.decode("#9cc634"));
        addButton.setForeground(Color.WHITE);
        AbstractAction tambahMatkul = new AbstractAction("Tambahkan"){
            public void actionPerformed(ActionEvent e) {

                String kode = kodeMatkulField.getText();
                String nama = namaMatkulField.getText();
                String sks = SKSField.getText();
                String kapasitas = kapasitasField.getText();

                if(kode.equals("") || nama.equals("") || sks.equals("") || kapasitas.equals("")) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field - TambahMatkul");
                    return;
                }
                Integer sksInt = Integer.valueOf(sks);
                Integer kapasitasInt = Integer.valueOf(kapasitas);
                MataKuliah matkul = new MataKuliah(kode, nama, sksInt, kapasitasInt);
                if(daftarMataKuliah.contains(matkul)) {
                    JOptionPane.showMessageDialog(frame, String.format("Mata Kuliah %s sudah pernah ditambahkan sebelumnya", nama));
                    kodeMatkulField.setText("");
                    namaMatkulField.setText("");
                    SKSField.setText("");
                    kapasitasField.setText("");
                }
                else {
                    daftarMataKuliah.add(matkul);
                    SistemAkademikGUI.sortMatkul();
                    TambahIRSGUI.UpdateMatkulMenu();
                    HapusIRSGUI.UpdateMatkulMenu();
                    RingkasanMataKuliahGUI.UpdateMatkulMenu();
                    JOptionPane.showMessageDialog(frame, String.format("Mata Kuliah %s berhasil ditambahkan", nama));
                    kodeMatkulField.setText("");
                    namaMatkulField.setText("");
                    SKSField.setText("");
                    kapasitasField.setText("");
                }
            }
        };
        addButton.setAction(tambahMatkul);
        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "tambah matkul");
        this.getActionMap().put("tambah matkul",tambahMatkul);

        // inisialisasi tombol backButton
        JButton backButton = new JButton("Kembali");
        backButton.setFont(SistemAkademikGUI.fontGeneral);
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setAlignmentY(Component.CENTER_ALIGNMENT);
        backButton.setBackground(Color.decode("#84aecc"));
        backButton.setForeground(Color.WHITE);
        // inisialisasi back yang merupakan action yang akan ditambahkan ke tombolnya 
        // dan di-bind ke escape key
        AbstractAction back = new AbstractAction("Kembali") {
            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) HomeGUI.masterPanel.getLayout();
                cl.show(HomeGUI.masterPanel, "home panel");
            }
        };
        backButton.setAction(back);
        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), "kembali");
        this.getActionMap().put("kembali", back);

        // menambahkan komponen-komponen ke TambahMataKuliahGUI
        this.add(titleLabel);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(kodeMatkulLabel);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(kodeMatkulField);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(namaMatkulLabel);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(namaMatkulField);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(SKSLabel);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(SKSField);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(kapasitasLabel);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(kapasitasField);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(addButton);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(backButton);
    }
    
}
