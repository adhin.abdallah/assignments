package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI extends JPanel{
    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        String nama = mataKuliah.getNama();
        String kode = mataKuliah.getKode();
        int sks = mataKuliah.getSKS();
        int jumlahMahasiswa = mataKuliah.getJumlahMahasiswa();
        int kapasitas = mataKuliah.getKapasitas();
        Mahasiswa[] mahasiswaArr = mataKuliah.getDaftarMahasiswa();

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setAlignmentY(Component.CENTER_ALIGNMENT);

        JLabel titleLabel = new JLabel("Detail Ringkasan Mata Kuliah");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(titleLabel);

        JLabel name = new JLabel("Nama mata kuliah:" + nama);
        name.setHorizontalAlignment(JLabel.CENTER);
        name.setAlignmentX(Component.CENTER_ALIGNMENT);
        name.setAlignmentY(Component.CENTER_ALIGNMENT);
        name.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(name);

        JLabel kodeLabel = new JLabel("Kode:" + kode);
        kodeLabel.setHorizontalAlignment(JLabel.CENTER);
        kodeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        kodeLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        kodeLabel.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(kodeLabel);

        JLabel SKSLabel = new JLabel("SKS:" + sks);
        SKSLabel.setHorizontalAlignment(JLabel.CENTER);
        SKSLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        SKSLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        SKSLabel.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(SKSLabel);

        JLabel jumlahMahasiswaLabel = new JLabel("Jumlah mahasiswa:" + jumlahMahasiswa);
        jumlahMahasiswaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        jumlahMahasiswaLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        jumlahMahasiswaLabel.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(jumlahMahasiswaLabel);

        JLabel kapasitasLabel = new JLabel("Kapasitas:" + kapasitas);
        kapasitasLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        kapasitasLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        kapasitasLabel.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(kapasitasLabel);

        JLabel daftarMatkulLabel = new JLabel("Daftar Mahasiswa");
        daftarMatkulLabel.setHorizontalAlignment(JLabel.CENTER);
        daftarMatkulLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        daftarMatkulLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        daftarMatkulLabel.setFont(SistemAkademikGUI.fontGeneral);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(daftarMatkulLabel);

        if(mataKuliah.getJumlahMahasiswa() == 0) {
            JLabel noMahasiswa = new JLabel("Belum ada mahasiswa yang mengambil mata kuliah ini.");
            noMahasiswa.setHorizontalAlignment(JLabel.CENTER);
            noMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
            noMahasiswa.setAlignmentY(Component.CENTER_ALIGNMENT);
            noMahasiswa.setFont(new Font("Century Gothic", Font.BOLD , 14));
            this.add(Box.createRigidArea(new Dimension(5,10)));
            this.add(noMahasiswa);
        }
        else {
            int counter = 0; 
            for(Mahasiswa e : mahasiswaArr) {
                if(e != null){
                    String mahasiswaString = (counter+1) + ". " + e.getNama();
                    counter += 1;

                    JLabel matkulList = new JLabel(mahasiswaString);
                    matkulList.setAlignmentX(Component.CENTER_ALIGNMENT);
                    matkulList.setAlignmentY(Component.CENTER_ALIGNMENT);
                    matkulList.setFont(new Font("Century Gothic", Font.BOLD , 14));
                    this.add(Box.createRigidArea(new Dimension(5,10)));
                    this.add(matkulList);
                }
            }    
        }

        JButton doneButton = new JButton("Selesai");
        doneButton.setFont(SistemAkademikGUI.fontGeneral);
        doneButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        doneButton.setAlignmentY(Component.CENTER_ALIGNMENT);
        doneButton.setBackground(Color.decode("#9cc634"));
        doneButton.setForeground(Color.WHITE);
        doneButton.addActionListener( new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) HomeGUI.masterPanel.getLayout();
                cl.show(HomeGUI.masterPanel, "home panel");
            }
        });
        // membind enter key untuk meng-click tombol doneButton
        // tetapi gagal implementasinya karena walaupun sudah menggunkaan .grabFocus
        // fous tetap tidak berpindah ke doneButton
        doneButton.addKeyListener( new KeyListener() {
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                    doneButton.doClick();
                }
            }

            public void keyTyped(KeyEvent e) {
                
            }

            public void keyReleased(KeyEvent e) {
                
            }
        });
        doneButton.grabFocus();
        
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(doneButton);
    }
}