package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HapusIRSGUI extends JPanel{

    protected static JComboBox NPMMenu;
    protected static JComboBox matkulMenu;

    public HapusIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // menentukan Layout dari HapusIRSGUI
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setAlignmentY(Component.CENTER_ALIGNMENT);

        // inisialisasi label sambutan
        JLabel sambutan = new JLabel("Hapus IRS");
        sambutan.setHorizontalAlignment(JLabel.CENTER);
        sambutan.setAlignmentX(Component.CENTER_ALIGNMENT);
        sambutan.setAlignmentY(Component.CENTER_ALIGNMENT);
        sambutan.setFont(SistemAkademikGUI.fontTitle);

        // inisialisasi label pilihNPM
        JLabel pilihNPM = new JLabel();
        pilihNPM.setText("Pilih NPM");
        pilihNPM.setHorizontalAlignment(JLabel.CENTER);
        pilihNPM.setAlignmentX(Component.CENTER_ALIGNMENT);
        pilihNPM.setAlignmentY(Component.CENTER_ALIGNMENT);
        pilihNPM.setFont(SistemAkademikGUI.fontGeneral);

        // inisialisasi combobox NPMMenu
        Object[] dftrMahasiswa = daftarMahasiswa.toArray();
        NPMMenu = new JComboBox<>(dftrMahasiswa);
        NPMMenu.setMaximumSize(new Dimension(sambutan.getPreferredSize().width, pilihNPM.getPreferredSize().height));

        // inisialisasi label pilihMatkul
        JLabel pilihMatkul = new JLabel();
        pilihMatkul.setText("Pilih Nama Matkul");
        pilihMatkul.setHorizontalAlignment(JLabel.CENTER);
        pilihMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        pilihMatkul.setAlignmentY(Component.CENTER_ALIGNMENT);
        pilihMatkul.setFont(SistemAkademikGUI.fontGeneral);

        // inisialisasi combobox matkulMenu
        Object[] dftrMatkul = daftarMataKuliah.toArray();
        matkulMenu = new JComboBox<>(dftrMatkul);
        matkulMenu.setFont(new Font("Century Gothic", Font.BOLD , 14));
        matkulMenu.setMaximumSize(new Dimension(pilihNPM.getPreferredSize().width, sambutan.getPreferredSize().height));

        // inisialisasi tombol deleteButton
        JButton deleteButton = new JButton("Hapus");
        deleteButton.setFont(SistemAkademikGUI.fontGeneral);
        deleteButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        deleteButton.setAlignmentY(Component.CENTER_ALIGNMENT);
        deleteButton.setBackground(Color.decode("#9cc634"));
        deleteButton.setForeground(Color.WHITE);
        // inisialisasi hapusIRS yang merupakan action yang akan ditambahkan ke tombolnya 
        // dan di-bind ke enter key
        AbstractAction hapusIRS = new AbstractAction("Hapus"){
            @Override
            public void actionPerformed(ActionEvent e) {
                String npm = (String) NPMMenu.getSelectedItem();
                String matkul = (String) matkulMenu.getSelectedItem();

                if(npm == null || matkul == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    return;
                }
                Long npmLong = Long.parseLong(npm);
                Mahasiswa s = this.getMahasiswa(npmLong);
                MataKuliah m = this.getMataKuliah(matkul);
                JOptionPane.showMessageDialog(frame, s.dropMatkul(m));
            }

            private MataKuliah getMataKuliah(String nama) {

                for (MataKuliah mataKuliah : SistemAkademikGUI.daftarMataKuliah) {
                    if (mataKuliah.getNama().equals(nama)){
                        return mataKuliah;
                    }
                }
                return null;
            }
        
            private Mahasiswa getMahasiswa(long npm) {
        
                for (Mahasiswa mahasiswa : SistemAkademikGUI.daftarMahasiswa) {
                    if (mahasiswa.getNpm() == npm){
                        return mahasiswa;
                    }
                }
                return null;
            }
        };
        // manambahkan action hapusIRS ke showbutton
        deleteButton.setAction(hapusIRS);
        // inisialisasi KeyListener yang akan meng-klik deletebutton
        // apabila menerima key press enter key
        KeyListener listener =  new KeyListener() {
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                    deleteButton.doClick();
                }
            }
            public void keyTyped(KeyEvent e) {

            }

            public void keyReleased(KeyEvent e) {
 
            }
        };

        // menambahkan listener ke komponen pada HapusIRSGUI
        NPMMenu.addKeyListener(listener);
        matkulMenu.addKeyListener(listener);
        deleteButton.addKeyListener(listener);
    
        // inisialisasi tombol backButton
        JButton backButton = new JButton("Kembali");
        backButton.setFont(SistemAkademikGUI.fontGeneral);
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setAlignmentY(Component.CENTER_ALIGNMENT);
        backButton.setBackground(Color.decode("#84aecc"));
        backButton.setForeground(Color.WHITE);
        // inisialisasi back yang merupakan action yang akan ditambahkan ke tombolnya 
        // dan di-bind ke escape key
        AbstractAction back = new AbstractAction("Kembali") {
            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) HomeGUI.masterPanel.getLayout();
                cl.show(HomeGUI.masterPanel, "home panel");
            }
        };
        backButton.setAction(back);
        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), "kembali");
        this.getActionMap().put("kembali", back);

        // menambahkan komponen-komponen ke HapusIRSGUI
        this.add(Box.createRigidArea(new Dimension(5,50)));
        this.add(sambutan);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(pilihNPM);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(NPMMenu);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(pilihMatkul);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(matkulMenu);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(deleteButton);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(backButton);
    }

    // method untuk mengupdate value yang ditunjukkan NPMMenu
    public static void UpdateStudentMenu() {
        ArrayList<Mahasiswa> arr1 =SistemAkademikGUI.daftarMahasiswa;
        DefaultComboBoxModel model = (DefaultComboBoxModel) HapusIRSGUI.NPMMenu.getModel();
        model.removeAllElements();

        for (Mahasiswa e : arr1) {
            model.addElement(e.toString());
        }

        NPMMenu.setModel(model);
    }

    // method untuk mengupdate value yang ditunjukkan matkulMenu
    public static void UpdateMatkulMenu() {
        ArrayList<MataKuliah> arr1 = SistemAkademikGUI.daftarMataKuliah;
        DefaultComboBoxModel model = (DefaultComboBoxModel) HapusIRSGUI.matkulMenu.getModel();
        model.removeAllElements();

        for (MataKuliah e : arr1) {
            model.addElement(e.toString());
        }
        HapusIRSGUI.matkulMenu.getModel();
    }
}