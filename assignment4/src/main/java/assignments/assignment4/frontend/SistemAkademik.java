package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class SistemAkademik {
    public static void main(String[] args) { 
        new SistemAkademikGUI();
    }
}

class SistemAkademikGUI extends JFrame{
    protected static ArrayList<Mahasiswa> daftarMahasiswa = new ArrayList<Mahasiswa>();
    protected static ArrayList<MataKuliah> daftarMataKuliah = new ArrayList<MataKuliah>();
    public static Font fontGeneral = new Font("Century Gothic", Font.PLAIN , 14);
    public static Font fontTitle = new Font("Century Gothic", Font.BOLD, 20);


    public SistemAkademikGUI(){

        // Membuat Frame
        JFrame frame = new JFrame("Administrator - Sistem Akademik");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);  
        
        //inisialisasi HomeGUI
        new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
        frame.setVisible(true);

    }

    //sorting untuk mahasiswa
    public static void sortMahasiswa() {
        for (int i = 1; i < daftarMahasiswa.size(); i++) {
            Mahasiswa current = daftarMahasiswa.get(i);
            int j = i - 1;
            while(j >= 0 && current.getNpm() < daftarMahasiswa.get(j).getNpm()) {
                daftarMahasiswa.set(j+1,daftarMahasiswa.get(j));
                j--;
            }
            daftarMahasiswa.set(j+1, current);
        }
    }

    //sorting untuk mata kuliah
    public static void sortMatkul() {
        for (int i = 1; i < daftarMataKuliah.size(); i++) {
            MataKuliah current = daftarMataKuliah.get(i);
            int j = i - 1;
            while(j >= 0 && 0 > current.toString().compareToIgnoreCase(daftarMataKuliah.get(i).toString())) {
                daftarMataKuliah.set(j+1,daftarMataKuliah.get(j));
                j--;
            }
            daftarMataKuliah.set(j+1, current);
        }
    }
}
