package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahIRSGUI extends JPanel{

    public static JLabel titleLabel;
    protected static JComboBox NPMMenu;
    protected static JComboBox matkulMenu;

    public TambahIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // menentukan layout dari TambahIRSGUI
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setAlignmentY(Component.CENTER_ALIGNMENT);

        // inisialisasi label titleLabel
        titleLabel = new JLabel("Tambah IRS");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // inisialisasi label pilihNPM
        JLabel pilihNPM = new JLabel();
        pilihNPM.setText("Pilih NPM");
        pilihNPM.setHorizontalAlignment(JLabel.CENTER);
        pilihNPM.setAlignmentX(Component.CENTER_ALIGNMENT);
        pilihNPM.setAlignmentY(Component.CENTER_ALIGNMENT);
        pilihNPM.setFont(SistemAkademikGUI.fontGeneral);
        JScrollBar sBar = new JScrollBar();
        pilihNPM.add(sBar);

        // inisialisasi combobox untuk memilih NPM
        Object[] dftrMahasiswa = new ArrayList<Object>().toArray();
        NPMMenu = new JComboBox(dftrMahasiswa);
        NPMMenu.setMaximumSize(new Dimension(titleLabel.getPreferredSize().width, pilihNPM.getPreferredSize().height));

        // inisialisasi label pilihMatkul
        JLabel pilihMatkul = new JLabel();
        pilihMatkul.setText("Pilih Nama Matkul");
        pilihMatkul.setHorizontalAlignment(JLabel.CENTER);
        pilihMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        pilihMatkul.setAlignmentY(Component.CENTER_ALIGNMENT);
        pilihMatkul.setFont(SistemAkademikGUI.fontGeneral);

        // inisialisasi combobox untuk memilih matkul
        Object[] dftrMatkul = daftarMataKuliah.toArray();
        matkulMenu = new JComboBox<>(dftrMatkul);
        matkulMenu.setMaximumSize(new Dimension(pilihNPM.getPreferredSize().width, titleLabel.getPreferredSize().height));

        // inisialisasi tombol addButton
        JButton addButton = new JButton("Tambahkan");
        addButton.setFont(SistemAkademikGUI.fontGeneral);
        addButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        addButton.setAlignmentY(Component.CENTER_ALIGNMENT);
        addButton.setBackground(Color.decode("#9cc634"));
        addButton.setForeground(Color.WHITE);
        // inisialisasi tambahIRS yang merupakan action yang akan ditambahkan ke tombolnya 
        // dan di-bind ke enter key
        AbstractAction tambahIRS = new AbstractAction("Tambahkan"){
            public void actionPerformed(ActionEvent e) {

                String npm = (String) NPMMenu.getSelectedItem();
                String matkul = (String) matkulMenu.getSelectedItem();
            
                if(npm == null || matkul == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field - TambahIRS");
                    return;
                }
                Long npmLong = Long.parseLong(npm);
                Mahasiswa s = this.getMahasiswa(npmLong);
                MataKuliah m = this.getMataKuliah(matkul);
                JOptionPane.showMessageDialog(frame, s.addMatkul(m));
            }

            private MataKuliah getMataKuliah(String nama) {

                for (MataKuliah mataKuliah : SistemAkademikGUI.daftarMataKuliah) {
                    if (mataKuliah.getNama().equals(nama)){
                        return mataKuliah;
                    }
                }
                return null;
            }
        
            private Mahasiswa getMahasiswa(long npm) {
        
                for (Mahasiswa mahasiswa : SistemAkademikGUI.daftarMahasiswa) {
                    if (mahasiswa.getNpm() == npm){
                        return mahasiswa;
                    }
                }
                return null;
            }
        };

        // manambahkan action tambahIRS ke addButton
        addButton.setAction(tambahIRS);
        // mem-bind enter key dengan tambahIRS
        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "tambah irs");
        this.getActionMap().put("tambah irs", tambahIRS);

        // inisialisasi tombol backButton
        JButton backButton = new JButton("Kembali");
        backButton.setFont(SistemAkademikGUI.fontGeneral);
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setAlignmentY(Component.CENTER_ALIGNMENT);
        backButton.setBackground(Color.decode("#84aecc"));
        backButton.setForeground(Color.WHITE);
        // inisialisasi back yang merupakan action yang akan ditambahkan ke tombolnya 
        // dan di-bind ke escape key
        AbstractAction back = new AbstractAction("Kembali") {
            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) HomeGUI.masterPanel.getLayout();
                cl.show(HomeGUI.masterPanel, "home panel");
            }
        };
        backButton.setAction(back);
        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), "kembali");
        this.getActionMap().put("kembali", back);

        // menambahkan komponen-komponen ke TambahIRSGUI
        this.add(Box.createRigidArea(new Dimension(5,50)));
        this.add(titleLabel);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(pilihNPM);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(NPMMenu);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(pilihMatkul);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(matkulMenu);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(addButton);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(backButton);

    }

    // method untuk mengupdate value yang ditunjukkan oleh NPMMenu
    public static void UpdateStudentMenu() {
        ArrayList<Mahasiswa> arr1 =SistemAkademikGUI.daftarMahasiswa;
        DefaultComboBoxModel model = (DefaultComboBoxModel) TambahIRSGUI.NPMMenu.getModel();
        model.removeAllElements();

        for (Mahasiswa e : arr1) {
            model.addElement(e.toString());
        }

        TambahIRSGUI.NPMMenu.setModel(model);
    }

    // method untuk mengupdate value yang ditunjukkan oleh matkulMenu
    public static void UpdateMatkulMenu() {
        ArrayList<MataKuliah> arr1 = SistemAkademikGUI.daftarMataKuliah;
        DefaultComboBoxModel model = (DefaultComboBoxModel) TambahIRSGUI.matkulMenu.getModel();
        model.removeAllElements();

        for (MataKuliah e : arr1) {
            model.addElement(e.toString());
        }

        TambahIRSGUI.matkulMenu.getModel();
    }
}