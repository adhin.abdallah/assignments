package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI extends JPanel{

    protected static JComboBox NPMMenu;

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // menentukan Layout dari RingkasanMahasiswaGUI
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setAlignmentY(Component.CENTER_ALIGNMENT);

        // inisialisasi label titleLabel
        JLabel titleLabel = new JLabel("Ringkasan Mahasiswa");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        titleLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // inisialisasi label pilihNPM
        JLabel pilihNPM = new JLabel();
        pilihNPM.setText("Pilih NPM");
        pilihNPM.setHorizontalAlignment(JLabel.CENTER);
        pilihNPM.setAlignmentX(Component.CENTER_ALIGNMENT);
        pilihNPM.setAlignmentY(Component.CENTER_ALIGNMENT);
        pilihNPM.setFont(SistemAkademikGUI.fontGeneral);

        // inisialisasi Combobox NPMMenu
        Object[] dftrMahasiswa = daftarMahasiswa.toArray();
        NPMMenu = new JComboBox<>(dftrMahasiswa);
        NPMMenu.setMaximumSize(new Dimension(titleLabel.getPreferredSize().width-80, pilihNPM.getPreferredSize().height+6));

        // inisialisasi tombol showButton
        JButton showButton = new JButton("Lihat");
        showButton.setFont(SistemAkademikGUI.fontGeneral);
        showButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        showButton.setAlignmentY(Component.CENTER_ALIGNMENT);
        showButton.setBackground(Color.decode("#9cc634"));
        showButton.setForeground(Color.WHITE);
        // inisialisasi lihatMahasiswa yang merupakan action yang akan ditambahkan ke tombolnya 
        // dan di-bind ke enter key
        AbstractAction lihatMahasiswa = new AbstractAction("Lihat"){
                public void actionPerformed(ActionEvent e) {
    
                    String npm = (String) NPMMenu.getSelectedItem();
                    
                    if(npm == null) {
                        JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field - RingkasanMahasiswa");
                        return;
                    }
                    Long npmLong = Long.parseLong(npm);
                    Mahasiswa s = this.getMahasiswa(npmLong);
                    DetailRingkasanMahasiswaGUI ringkasan = new DetailRingkasanMahasiswaGUI(frame, s, daftarMahasiswa, daftarMataKuliah);
                    HomeGUI.masterPanel.add(ringkasan, "detail ringkasan mahasiswa");
                    CardLayout cl = (CardLayout) HomeGUI.masterPanel.getLayout();
                    cl.show(HomeGUI.masterPanel, "detail ringkasan mahasiswa");
                }
                private Mahasiswa getMahasiswa(Long npm) {
                    for (Mahasiswa mahasiswa : SistemAkademikGUI.daftarMahasiswa) {
                        if (mahasiswa.getNpm() == npm){
                            return mahasiswa;
                        }
                    }
                    return null;
                }
        };
        // manambahkan action lihatMahasiswa ke showbutton
        showButton.setAction(lihatMahasiswa);
        // mem-bind enter key dengan lihatMahasiswa
        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "lihat mahasiswa");
        this.getActionMap().put("lihat mahasiswa",lihatMahasiswa);

        // inisialisasi tombol backButton
        JButton backButton = new JButton("Kembali");
        backButton.setFont(SistemAkademikGUI.fontGeneral);
        backButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        backButton.setAlignmentY(Component.CENTER_ALIGNMENT);
        backButton.setBackground(Color.decode("#84aecc"));
        backButton.setForeground(Color.WHITE);
        // inisialisasi back yang merupakan action yang akan ditambahkan ke tombolnya 
        // dan di-bind ke escape key
        AbstractAction back = new AbstractAction("Kembali") {
            public void actionPerformed(ActionEvent e) {
                CardLayout cl = (CardLayout) HomeGUI.masterPanel.getLayout();
                cl.show(HomeGUI.masterPanel, "home panel");
            }
        };
        backButton.setAction(back);
        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), "kembali");
        this.getActionMap().put("kembali", back);

        // menambahkan komponen-komponen ke RingkasanMahasiswaGUI
        this.add(titleLabel);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(pilihNPM);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(NPMMenu);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(showButton);
        this.add(Box.createRigidArea(new Dimension(5,10)));
        this.add(backButton);
    }

    // method untuk mengupdate value yang ditunjukkan NPMMenu
    public static void UpdateStudentMenu() {
        ArrayList<Mahasiswa> arr1 =SistemAkademikGUI.daftarMahasiswa;
        DefaultComboBoxModel model = (DefaultComboBoxModel) RingkasanMahasiswaGUI.NPMMenu.getModel();
        model.removeAllElements();

        for (Mahasiswa e : arr1) {
            model.addElement(e.toString());
        }

        RingkasanMahasiswaGUI.NPMMenu.setModel(model);
    }
}