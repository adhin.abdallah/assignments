package assignments.assignment3;

class Makanan {

    private String nama;

    private long harga;

    //constructor untuk class Makanan
    public Makanan(String nama, long harga) {
        this.nama = nama;
        this.harga = harga;
    }

    //getter untuk harga makanan
    public long getHarga() {
        return this.harga;
    }

    public String toString() {
        return this.nama;
    }
}