package assignments.assignment3;

import java.util.Arrays;
import java.util.Comparator;

public abstract class ElemenFasilkom {

    private String tipe;
    
    private String nama;

    private int friendship;

    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    private int telahMenyapaCounter;

    //constructor untuk ElemenFasilkom
    public ElemenFasilkom(String nama, String tipe) {
        this.nama = nama;
        this.tipe = tipe;
    }

    //getter untuk tipe
    public String getTipe() {
        return this.tipe;
    }

    //getter untuk Friendship
    public int getFriendship() {
        return this.friendship;
    }

    //setter untuk friendship
    public void setFriendship(int num) {
        //if-else agar nilai friendship hanya pada rentang 0 <= x <= 100
        if(this.friendship + num > 100) {
            this.friendship = 100;
        }
        else if(this.friendship + num < 0) {
            this.friendship = 0;
        }
        else {
            this.friendship += num;
        }
    }

    //getter untuk array telahMenyapa
    public ElemenFasilkom[] getTelahMenyapa() {
        return this.telahMenyapa;
    }

    //getter untuk telahMenyapaCounter
    public int getTelahMenyapaCounter() {
        return this.telahMenyapaCounter;
    }

    //setter untuk telahMenyapaCounter
    public void setTelahMenyapaCounter(int num) {
        this.telahMenyapaCounter += num;
    }

    //method untuk menyapa elemenfasilkom lainnya
    public void menyapa(ElemenFasilkom elemenFasilkom) {
        //mengecek apakah elemenfasilkom telah disapa atau belum
        if(Arrays.asList(this.telahMenyapa).contains(elemenFasilkom)) {
            System.out.println(String.format("[DITOLAK] %s telah menyapa %s hari ini", this.nama, elemenFasilkom));
        }
        else {
            //if-else jika mahasiswa dan dosen saling menyapa
            if(this.tipe.equals("mahasiswa") && elemenFasilkom.getTipe().equals("dosen")) {
                Mahasiswa mahasiswa = (Mahasiswa) this;
                Dosen dosen = (Dosen) elemenFasilkom;
                if(dosen.getMataKuliah() == null) {
                    
                }
                else {
                    if(Arrays.asList(mahasiswa.getDaftarMKuliah()).contains(dosen.getMataKuliah())) {
                        this.friendship += 2;
                        dosen.setFriendship(2);
                    }
                }
            }
            else if(this.tipe.equals("dosen") && elemenFasilkom.getTipe().equals("mahasiswa")) {
                Mahasiswa mahasiswa = (Mahasiswa) elemenFasilkom;
                Dosen dosen = (Dosen) this;
                if(dosen.getMataKuliah() == null) {
                    
                }
                else {
                    if(Arrays.asList(mahasiswa.getDaftarMKuliah()).contains(dosen.getMataKuliah())) {
                        this.friendship += 2;
                        mahasiswa.setFriendship(2);
                    }
                }
            }
            this.telahMenyapa[telahMenyapaCounter] = elemenFasilkom;
            this.telahMenyapaCounter += 1;

            elemenFasilkom.getTelahMenyapa()[elemenFasilkom.getTelahMenyapaCounter()] = this;
            elemenFasilkom.setTelahMenyapaCounter(1);
            System.out.println(String.format("%s menyapa dengan %s", this.nama, elemenFasilkom));
        }
    }

    //method untuk me-reset telahMenyapa
    public void resetMenyapa() {
        telahMenyapa = new ElemenFasilkom[100];
        telahMenyapaCounter = 0;
    }

    //method untuk membeli makanan dari ElemenKantin
    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        ElemenKantin seller = (ElemenKantin) penjual;
        //for-loop untuk mencari makanan
        for(Makanan food : seller.getDaftarMakanan()) {
            if(food == null) {
                continue;
            }
            else if(food.toString().equals(namaMakanan)) {
                this.friendship += 1;
                penjual.setFriendship(1);
                System.out.println(String.format("%s berhasil membeli %s seharga %d", pembeli, namaMakanan, food.getHarga()));
                return;
            }
        }
        System.out.println(String.format("[DITOLAK] %s tidak menjual %s", penjual, namaMakanan));
    }

    //comparator unutk ElemenFasilkom
    public static int compareTo(ElemenFasilkom orangA, ElemenFasilkom orangB) {
        if(orangA == null || orangB == null) {
            return 0;
        }
        else {
            int friendshipOrangA = orangA.getFriendship();
            int friendshipOrangB = orangB.getFriendship();

            if(friendshipOrangA > friendshipOrangB) {
                return -1;
            }
            else if(friendshipOrangA == friendshipOrangB) {
                return orangA.toString().compareTo(orangB.toString());
            }
            else {
                return 1;
            }
        }
    }
    
    public String toString() {  
        return this.nama;
    }
}