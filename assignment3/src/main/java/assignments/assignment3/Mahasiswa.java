package assignments.assignment3;

import java.util.Arrays;

class Mahasiswa extends ElemenFasilkom {

    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];

    private int jumlahMataKuliah;
    
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;

    //constructor dengan inheritance untuk class Dosen
    public Mahasiswa(String nama, long npm) {
        super(nama, "mahasiswa");
        this.npm = npm;
    }

    //getter untuk NPM
    public long getNPM() {
        return this.npm;
    }

    //getter untuk array daftarMataKuliah
    public MataKuliah[] getDaftarMKuliah() {
        return this.daftarMataKuliah;
    }

    //getter untuk jumlahMatkul
    public int getJumlahMatkul() {
        return this.jumlahMataKuliah;
    }

    //method untuk menambahkan MataKuliah yang dipelajari Mahasiswa
    public void addMatkul(MataKuliah mataKuliah) {
        if(!Arrays.asList(this.daftarMataKuliah).contains(mataKuliah)) {
            if(mataKuliah.getJumlahMahasiswa()+1 <= mataKuliah.getKapasitas()) {
                    for(int i = 0; i<10; i++) {
                        if(this.daftarMataKuliah[i] == null) {
                            this.daftarMataKuliah[i] = mataKuliah;
                            this.jumlahMataKuliah += 1;
                            mataKuliah.addMahasiswa(this);
                            System.out.println(String.format("%s berhasil menambahkan mata kuliah %s", this, mataKuliah));
                            return;
                        }
                    }
            } 
            else {
                System.out.println(String.format("[DITOLAK] %s telah penuh kapasitasnya",mataKuliah));
            }
        } 
        else {
            System.out.println("[DITOLAK] " + mataKuliah +" telah diambil sebelumnya");
        }
    }

    //method untuk men-drop MataKuliah yang dipelajari Mahasiswa
    public void dropMatkul(MataKuliah mataKuliah) {
        if(Arrays.asList(this.daftarMataKuliah).contains(mataKuliah)) {
            for(int i = 0; i<10; i++) {
                if(this.daftarMataKuliah[i] == mataKuliah) {
                    this.daftarMataKuliah[i] = null;
                    this.jumlahMataKuliah -= 1;
                    mataKuliah.dropMahasiswa(this);
                    System.out.println(String.format("%s berhasil drop mata kuliah %s", this, mataKuliah));
                }
            }
        } 
        else {
            System.out.println("[DITOLAK] " + mataKuliah + " belum pernah diambil");
        }
    }

    //method untuk mengambil tanggal lahir dari NPM
    public String extractTanggalLahir(long npm) {
        String npmString = Long.toString(npm);

        String strDate = Integer.toString(Integer.valueOf(npmString.substring(4,6)));
        String strMonth = Integer.toString(Integer.valueOf(npmString.substring(6,8)));
        String strYear = npmString.substring(8,12);

        this.tanggalLahir = strDate + "-" + strMonth + "-" + strYear;
        return this.tanggalLahir;
    }

    //method untuk mengambil jurusan seorang mahasiswa
    public String extractJurusan(long npm) {
        //if-else agar proses meng-extract jurursan dari NPM hanya dijalankan satu kali
        if(this.jurusan == null) {
            String facultyCodeString = Long.toString(npm).substring(2, 4);
            if(facultyCodeString.equals("01")) {
                this.jurusan = "Ilmu Komputer";
            } 
            else if(facultyCodeString.equals("02")){
                this.jurusan = "Sistem Informasi";
            }
            return this.jurusan;
        } 
        else {
            return this.jurusan;
        }
    }
}