package assignments.assignment3;

class MataKuliah {

    private String nama;
    
    private int kapasitas;

    private Dosen dosen;

    private Mahasiswa[] daftarMahasiswa;

    private int jumlahMahasiswa;

    //constructor untuk class MataKuliah
    public MataKuliah(String nama, int kapasitas) {
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[this.kapasitas];
    }

    //getter untuk dosen pengajar
    public String getDosen() {
        if(this.dosen != null) {
            return this.dosen.toString();
        }
        else {
            return "Belum ada";
        }
    }

    //getter untuk kapasitas matkul
    public int getKapasitas() {
        return this.kapasitas;
    }

    //getter untuk array daftarMahasiswa
    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }

    //getter untuk jumlahMahasiswa
    public int getJumlahMahasiswa() {
        return this.jumlahMahasiswa;
    }

    //method untuk menambahkan mahasiswa yang mengambil suatu matkul 
    public void addMahasiswa(Mahasiswa mahasiswa) {
        for(int i = 0; i<this.kapasitas; i++) {
            if(this.daftarMahasiswa[i] == null) {
                this.daftarMahasiswa[i] = mahasiswa;
                this.jumlahMahasiswa += 1;
                return;
            }
        }
    }

    //method untuk menghilangkan mahasiswa dari Array jika mahasiswa tersebut drop matkul
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        for(int i = 0; i<this.kapasitas; i++) {
            if(this.daftarMahasiswa[i] == mahasiswa) {
                this.daftarMahasiswa[i] = null;
                this.jumlahMahasiswa -= 1;
            }
        }
    }

    //method untuk meng-assign dosen pengajar
    public void addDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    //method untuk menghilangkan dosen pengajar
    public void dropDosen() {
        this.dosen = null;
    }

    public String toString() {
        return this.nama;
    }
}