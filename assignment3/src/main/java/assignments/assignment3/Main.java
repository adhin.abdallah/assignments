package assignments.assignment3;

import java.util.Scanner;
import java.util.Set;
import java.util.Arrays;
import java.util.Collections;

public class Main {

    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    private static int totalMataKuliah = 0;

    private static int totalElemenFasilkom = 0;
    
    //method untuk mencari ElemenFasilkom berdasarkan nama
    public static ElemenFasilkom searchByName(String nama) {
        for(ElemenFasilkom orang : daftarElemenFasilkom) {
            if(orang == null) {
                continue;
            }
            else if(nama.equals(orang.toString())) {
                return orang;
            }
        }
        return null;
    }

    //method untuk mencari MataKuliah berdasarkan nama
    public static MataKuliah searchMatkul(String nama) {
        for(MataKuliah matkul : daftarMataKuliah) {
            if(matkul == null) {
                continue;
            }
            else if(matkul.toString().equals(nama)){
                return matkul;
            }
        }
        return null;
    }

    //method untuk menambahkan Mahasiswa
    public static void addMahasiswa(String nama, long npm) {
        Mahasiswa newMahasiswa = new Mahasiswa(nama, npm);
        daftarElemenFasilkom[totalElemenFasilkom] = newMahasiswa;
        totalElemenFasilkom += 1;
        System.out.println(String.format("%s berhasil ditambahkan", nama));
    }

    //method untuk menambahkan Dosen
    public static void addDosen(String nama) {
        Dosen newDosen = new Dosen(nama);
        daftarElemenFasilkom[totalElemenFasilkom] = newDosen;
        totalElemenFasilkom += 1;
        System.out.println(String.format("%s berhasil ditambahkan", nama));
    }

    //method untuk menambahkan ElemenKantin
    public static void addElemenKantin(String nama) {
        ElemenKantin newElemenKantin = new ElemenKantin(nama);
        daftarElemenFasilkom[totalElemenFasilkom] = newElemenKantin;
        totalElemenFasilkom += 1;
        System.out.println(String.format("%s berhasil ditambahkan", nama));
    }

    //method untuk menyapa seorang elemen fasilkom
    public static void menyapa(String objek1, String objek2) {
        ElemenFasilkom person1 = searchByName(objek1);
        ElemenFasilkom person2 = searchByName(objek2);
        //mengecek apabila orang pertama dan kedua adalah orang yang sama atau bukan?
        if(person1 == person2) {
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        }
        else {
            person1.menyapa(person2);
        }
    }

    //method untuk meng-assign suatu makanan ke ElemenFasilkom
    public static void addMakanan(String objek, String namaMakanan, long harga) {
        ElemenFasilkom seller = searchByName(objek);

        //if-else untuk mengecek apakah orang yang akan ditambahkan makanan ElemenKantin atau bukan
        if(seller.getTipe().equals("elemen kantin")) {
            ElemenKantin penjual = (ElemenKantin) seller;
            penjual.setMakanan(namaMakanan, harga);
        }
        else {
            System.out.println(String.format("[DITOLAK] %s bukan merupakan elemen kantin", objek));
        }
    }

    //method untuk membeli makanan dari seorang ElemenFasilkom
    public static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        ElemenFasilkom pembeli = searchByName(objek1);
        ElemenFasilkom penjual = searchByName(objek2);

        //if-else untuk mengecek apakah penjual ElemenKantin atau bukan dan apakah pembeli dan penjual orang yang sama
        if(!penjual.getTipe().equals("elemen kantin")) {
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        }
        else if(pembeli == penjual) {
            System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
        }
        else {
            pembeli.membeliMakanan(pembeli, penjual, namaMakanan);
        }
    }

    //method untuk menambahakan matkul
    public static void createMatkul(String nama, int kapasitas) {
        MataKuliah matkul = new MataKuliah(nama, kapasitas);
        daftarMataKuliah[totalMataKuliah] = matkul;
        totalMataKuliah += 1;
        System.out.println(String.format("%s berhasil ditambahkan dengan kapasitas %d", nama, kapasitas));
    }


    //method untuk menambahkan matkul ke seorang Mahasiswa
    public static void addMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom orang = searchByName(objek);
        MataKuliah matkul = searchMatkul(namaMataKuliah);

        if(orang.getTipe().equals("mahasiswa")) {
            Mahasiswa student = (Mahasiswa) orang;
            student.addMatkul(matkul);
        }
        else {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        }
    }

    //method untuk men-drop matkul dai seorang Mahasiswa
    public static void dropMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom orang = searchByName(objek);
        MataKuliah matkul = searchMatkul(namaMataKuliah);

        if(orang.getTipe().equals("mahasiswa")) {
            Mahasiswa student = (Mahasiswa) orang;
            student.dropMatkul(matkul);
        }
        else {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
        }
    }

    //method untuk meng-assign dosen pengajar ke suatu matkul
    public static void mengajarMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom orang = searchByName(objek);
        MataKuliah matkul = searchMatkul(namaMataKuliah);

        //if-else untuk mengecek apakah ElemenFasilkom yang dicari dosen atau bukan
        if(orang.getTipe().equals("dosen")) {
            Dosen dosen = (Dosen) orang;
            dosen.mengajarMataKuliah(matkul);
        }
        else {
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        }
    }

    //method untuk men-drop dosen pengajar dari suatu matkul
    public static void berhentiMengajar(String objek) {
        ElemenFasilkom orang = searchByName(objek);

        //if-else untuk mengecek apakah ElemenFasilkom yang dicari dosen atau bukan
        if(orang.getTipe().equals("dosen")) {
            Dosen dosen = (Dosen) orang;
            dosen.dropMataKuliah();
        }
        else {
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        }
    }

    //method unutk mencetak ringkasan mahasiswa
    public static void ringkasanMahasiswa(String objek) {
        ElemenFasilkom person = searchByName(objek);
        if(!person.getTipe().equals("mahasiswa")) {
            System.out.println(String.format("[DITOLAK] %s bukan merupakan seorang mahasiswa",person));
        }
        else {
            Mahasiswa searchedMahasiswa = (Mahasiswa) person;
            long npm = searchedMahasiswa.getNPM();
            System.out.println("Nama: " + searchedMahasiswa);
            System.out.println("Tanggal lahir: " + searchedMahasiswa.extractTanggalLahir(npm));
            System.out.println("Jurusan: " + searchedMahasiswa.extractJurusan(npm));
            System.out.println("Daftar Mata Kuliah:");

            if(searchedMahasiswa.getJumlahMatkul() == 0) {
                System.out.println("Belum ada mata kuliah yang diambil");
            } else {
                int counter = 0; 
                for(MataKuliah matkul : searchedMahasiswa.getDaftarMKuliah()) {
                    if(matkul != null){
                        System.out.println((counter+1) + ". " + matkul);
                        counter += 1;
                    }
                }
            }
        }
    }

    //method untuk mencetak ringaksan matakuliah
    public static void ringkasanMataKuliah(String namaMataKuliah) {
        MataKuliah searchedMataKuliah = searchMatkul(namaMataKuliah);

        System.out.println("Nama mata kuliah: " + searchedMataKuliah);
        System.out.println("Jumlah mahasiswa: " + searchedMataKuliah.getJumlahMahasiswa());
        System.out.println("Kapasitas: " + searchedMataKuliah.getKapasitas());
        System.out.println("Dosen pengajar: " + searchedMataKuliah.getDosen());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");

        if(searchedMataKuliah.getJumlahMahasiswa() == 0) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        }else {
            int counter = 0;
            for(Mahasiswa mSiswa : searchedMataKuliah.getDaftarMahasiswa()) {
                if(mSiswa != null) {
                    System.out.println(counter+1 + ". " + mSiswa);
                    counter+=1;
                }
            }
        }
    }

    //method nextDay yang akan menambahkan atau mengurangi friendship suatu ElemenFasilkom
    public static void nextDay() {
        for(ElemenFasilkom e : daftarElemenFasilkom) {
            if(e != null) {
                if(e.getTelahMenyapaCounter() >= (totalElemenFasilkom-1)/(double)2) {
                    e.resetMenyapa();
                    e.setFriendship(10);
                }
                else {
                    e.resetMenyapa();
                    e.setFriendship(-5);
                }
            }
        }
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        friendshipRanking();
    }

    //method untuk mencetak ElemenFasilkom yang terurur berdasarkan nilai friendship
    public static void friendshipRanking() {
        Arrays.sort(daftarElemenFasilkom, (a, b) -> {
            return ElemenFasilkom.compareTo(a, b);
          });

        int counter = 1;
        for(ElemenFasilkom e : daftarElemenFasilkom) {
            if(e != null) {
                System.out.println(String.format("%d. %s(%d)", counter, e, e.getFriendship()));
                counter += 1;
            }
            else {
                continue;
            }
        }
    }

    //method programEnd yang mencetak nilai akhir friendship
    public static void programEnd() {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    }
}