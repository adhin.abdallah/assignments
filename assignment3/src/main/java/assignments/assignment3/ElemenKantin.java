package assignments.assignment3;

import java.util.Arrays;

class ElemenKantin extends ElemenFasilkom {
    
    private Makanan[] daftarMakanan = new Makanan[10];

    private int jumlahMakanan;

    //constructor dengan inheritance untuk class ElemenKantin
    public ElemenKantin(String nama) {
        super(nama, "elemen kantin");
    }

    //getter untuk array daftarMakanan
    public Makanan[] getDaftarMakanan() {
        return this.daftarMakanan;
    }

    //method untuk meng-assign suatu makanan ke ElemenKantin
    public void setMakanan(String nama, long harga) {
        Makanan newMakanan = new Makanan(nama, harga);

        if(Arrays.asList(daftarMakanan).contains(newMakanan)) {
            System.out.println(String.format("[DITOLAK]%s sudah pernah terdaftar", nama));
        }else {
            daftarMakanan[jumlahMakanan] = newMakanan;
            jumlahMakanan += 1;
            System.out.println(String.format("%s telah mendaftarkan makanan %s dengan harga %d", this, nama, harga));
        } 
    }
}