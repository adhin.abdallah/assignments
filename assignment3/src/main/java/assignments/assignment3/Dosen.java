package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    private MataKuliah mataKuliah;

    //constructor dengan inheritance untuk class Dosen
    public Dosen(String nama) {
        super(nama, "dosen");
    }

    //getter matakuliah yang sedang diajar
    public MataKuliah getMataKuliah() {
        return this.mataKuliah;
    }

    //method untuk menambahkan matkul yang akan diajar
    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        if(this.mataKuliah != null) {
            System.out.println(String.format("[DITOLAK] %s sudah mengajar mata kuliah %s", this, this.mataKuliah));
        }
        else if(!mataKuliah.getDosen().equals("Belum ada")) {
            System.out.println(String.format("[DITOLAK] %s sudah memiliki dosen pengajar", mataKuliah));
        }
        else {
            mataKuliah.addDosen(this);
            System.out.println(String.format("%s mengajar mata kuliah %s", this, mataKuliah));
            this.mataKuliah = mataKuliah;
        }
    }

    //method untuk drop matkul yang sedang diajar
    public void dropMataKuliah() {
        if(this.mataKuliah == null) {
            System.out.println(String.format("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun", this));
        }
        else {
            System.out.println(String.format("%s berhenti mengajar %s", this, mataKuliah));
            this.mataKuliah.dropDosen();
            this.mataKuliah = null;
        }
    }
}